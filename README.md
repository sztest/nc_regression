For those who have a hard time with change.  😄

The original textures for [NodeCore](/packages/Warr1024/nodecore/) (_before_ the 2020-02-18 texture updates), featuring a mix of programmer art and quick-and-dirty placeholder textures.  Some users may find these textures improve accessibility, e.g. they may be more distinguishable through visual impairments.

Also supports the `nc_rabbits`, `nc_stucco`, and `nc_exmachina` mods.